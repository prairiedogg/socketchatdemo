#import <Foundation/Foundation.h>


@class ChatMessage;


@interface ChatMessageDeserializer : NSObject

- (ChatMessage *)chatMessageWithString:(NSString *)string;

@end
