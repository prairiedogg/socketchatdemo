#import "ChatMessage.h"


@interface ChatMessage ()

@property (copy, nonatomic) NSString *handle;
@property (copy, nonatomic) NSString *message;

@end


@implementation ChatMessage

- (id)initWithHandle:(NSString *)handle message:(NSString *)message
{
    self = [super init];
    if (self) {
        self.handle = handle;
        self.message = message;
    }
    return self;
}

#pragma mark - NSObject

- (id)init
{
    [self doesNotRecognizeSelector:_cmd];
    return nil;
}

@end
