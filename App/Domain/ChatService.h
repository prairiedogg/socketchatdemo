#import <Foundation/Foundation.h>


@protocol ChatServiceObserver;
@class SRWebSocket;


@interface ChatService : NSObject

- (id)initWithWebSocket:(SRWebSocket *)webSocket;
- (void)addObserver:(id<ChatServiceObserver>)observer;
- (void)removeObserver:(id<ChatServiceObserver>)observer;

@end
