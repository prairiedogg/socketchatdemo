#import "ChatMessagePresenter.h"
#import "ChatMessage.h"


@implementation ChatMessagePresenter

- (void)presentChatMessage:(ChatMessage *)chatMessage inCell:(UITableViewCell *)cell
{
    NSString *fullMessage = [NSString stringWithFormat:@"%@: %@", [chatMessage handle], [chatMessage message]];
    cell.textLabel.text = fullMessage;
}

@end
