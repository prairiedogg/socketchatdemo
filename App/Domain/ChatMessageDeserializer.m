#import "ChatMessageDeserializer.h"
#import "ChatMessage.h"


@implementation ChatMessageDeserializer

- (ChatMessage *)chatMessageWithString:(NSString *)string
{
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *messageDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSString *message = messageDictionary[@"text"];
    NSString *handle = messageDictionary[@"handle"];
    ChatMessage *chatMessage = [[ChatMessage alloc] initWithHandle:handle message:message];
    return chatMessage;
}

@end
