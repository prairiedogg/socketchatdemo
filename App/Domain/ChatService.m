#import "ChatService.h"
#import "SRWebSocket.h"
#import "ChatServiceObserver.h"
#import "ChatMessageDeserializer.h"
#import "ChatMessage.h"


@interface ChatService () <SRWebSocketDelegate>

@property (strong, nonatomic) SRWebSocket *webSocket;
@property (strong, nonatomic) NSHashTable *observers;
@property (strong, nonatomic) ChatMessageDeserializer *deserializer;

@end


@implementation ChatService

- (id)initWithWebSocket:(SRWebSocket *)webSocket
{
    self = [super init];
    if (self) {
        self.webSocket = webSocket;
        self.webSocket.delegate = self;
        self.deserializer = [[ChatMessageDeserializer alloc] init];
        self.observers = [NSHashTable weakObjectsHashTable];
    }
    return self;
}

- (void)addObserver:(id)observer
{
    if (![self.observers containsObject:observer]) {
        [self.observers addObject:observer];
    }
}

- (void)removeObserver:(id <ChatServiceObserver>)observer {
    [self.observers removeObject:observer];
}

#pragma mark - <SRWebSocketDelegate>

- (void)webSocketDidOpen:(SRWebSocket *)webSocket
{
    NSLog(@"webSocketDidOpen:");
}

- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error.localizedDescription);
    self.webSocket = nil;
}

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message;
{
    ChatMessage *chatMessage = [self.deserializer chatMessageWithString:message];

    for (id<ChatServiceObserver> observer in self.observers) {
        [observer chatService:self didReceiveMessage:chatMessage];
    }
}

- (void)webSocket:(SRWebSocket *)webSocket
 didCloseWithCode:(NSInteger)code
           reason:(NSString *)reason
         wasClean:(BOOL)wasClean;
{
    NSLog(@"web socket closed %@ with code %d because '%@'",
          wasClean ? @"cleanly" : @"in an unclean manner",
          code,
          reason);
    self.webSocket = nil;
}

#pragma mark - NSObject

- (void)dealloc
{
    self.webSocket.delegate = nil;
    [self.webSocket close];
    self.webSocket = nil;
}

@end
