#import <Foundation/Foundation.h>


@class ChatMessage;


@protocol ChatServiceObserver <NSObject>

- (void)chatService:(id)service
  didReceiveMessage:(ChatMessage *)chatMessage;

@end
