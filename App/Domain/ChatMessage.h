#import <Foundation/Foundation.h>


@interface ChatMessage : NSObject

@property (copy, nonatomic, readonly) NSString *handle;
@property (copy, nonatomic, readonly) NSString *message;

- (id)initWithHandle:(NSString *)handle message:(NSString *)message;

@end
