#import <Foundation/Foundation.h>


@class ChatMessage;


@interface ChatMessagePresenter : NSObject

- (void)presentChatMessage:(ChatMessage *)chatMessage inCell:(UITableViewCell *)cell;

@end
