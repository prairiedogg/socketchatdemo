#import "AppDelegate.h"
#import "ChatController.h"
#import "ChatService.h"
#import "SRWebSocket.h"


@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSString *serviceAddress = @"ws://pd-socket-chat-demo-001.herokuapp.com/receive";
//    NSString *serviceAddress = @"ws://127.0.0.1:5000/receive";
    NSURL *url = [NSURL URLWithString:serviceAddress];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    SRWebSocket *webSocket = [[SRWebSocket alloc] initWithURLRequest:request];
    ChatService *chatService = [[ChatService alloc] initWithWebSocket:webSocket];
    [webSocket open];

    ChatController *controller = [[ChatController alloc] initWithChatService:chatService];

    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:controller];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    self.window.rootViewController = navController;
    [self.window makeKeyAndVisible];
    return YES;
}

@end
