#import "ChatController.h"
#import "ChatMessagePresenter.h"
#import "ChatService.h"


@interface ChatController () <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *chats;
@property (strong, nonatomic) ChatMessagePresenter *presenter;
@property(nonatomic, strong) ChatService *chatService;

@end


static NSString *const cellIdentifier = @"!";


@implementation ChatController

- (id)initWithChatService:(ChatService *)chatService
{
    self = [super init];
    if (self) {
        self.chats = [NSMutableArray array];
        self.chatService = chatService;
        self.presenter = [[ChatMessagePresenter alloc] init];
    }
    return self;
}

#pragma mark - UIViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = @"Chat";

    UIBarButtonItem *item;
    item = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh
                                                         target:self
                                                         action:@selector(refresh:)];
    self.navigationItem.rightBarButtonItem = item;

    self.tableView = [[UITableView alloc] init];
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:cellIdentifier];
    [self.view addSubview:self.tableView];
    [self addConstraints];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.chatService addObserver:self];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.chatService removeObserver:self];
}

#pragma mark - <UITableViewDataSource>

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.chats.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
}

#pragma mark - <UITableViewDelegate>

-(void)tableView:(UITableView *)tableView
 willDisplayCell:(UITableViewCell *)cell
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    ChatMessage *chatMessage = self.chats[indexPath.row];
    [self.presenter presentChatMessage:chatMessage inCell:cell];
}

#pragma mark - <ChatServiceObserver>

- (void)chatService:(id)service
  didReceiveMessage:(ChatMessage *)chatMessage
{
    [self.chats addObject:chatMessage];
    [self.tableView reloadData];
    NSUInteger chatCount = (self.chats.count - 1);
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:chatCount inSection:0];
    [self.tableView scrollToRowAtIndexPath:indexPath
                          atScrollPosition:UITableViewScrollPositionTop
                                  animated:YES];
}

#pragma mark - Actions

- (void)refresh:(id)sender
{
    [self.chats removeAllObjects];
    [self.tableView reloadData];
}

#pragma mark - Private

- (void)addConstraints
{
    NSDictionary *views = @{@"tableView": self.tableView};
    NSString *h = @"H:|[tableView]|";
    NSString *v = @"V:|[tableView]|";
    NSArray *horizontalConstraints = [NSLayoutConstraint constraintsWithVisualFormat:h
                                                                             options:NSLayoutFormatDirectionLeadingToTrailing
                                                                             metrics:nil
                                                                               views:views];
    NSArray *verticalConstraints = [NSLayoutConstraint constraintsWithVisualFormat:v
                                                                           options:NSLayoutFormatDirectionLeadingToTrailing
                                                                           metrics:nil
                                                                             views:views];
    [self.view addConstraints:horizontalConstraints];
    [self.view addConstraints:verticalConstraints];
}

@end
