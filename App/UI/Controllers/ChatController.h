#import <UIKit/UIKit.h>
#import "ChatServiceObserver.h"


@class ChatService;


@interface ChatController : UIViewController <ChatServiceObserver>

- (id)initWithChatService:(ChatService *)chatService;

@end
